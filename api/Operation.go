package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type Operation struct {
	Number1   float64   `json:"number1" bson:"number2"`
	Number2   float64   `json:"number2" bson:"number2"`
	Operation string    `json:"operation" bson:"operation"`
	Result    float64   `json:"result" bson:"result"`
	Date      time.Time `json:"date" bson:"date"`
}

var operations = map[string]Operation{}

func InformationHandleFunc(w http.ResponseWriter, r *http.Request) {
	switch method := r.Method; method {

	case http.MethodPost:
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Erick Daniel Poron Munoz - 201712132 "))
	default:
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Unsupported request method."))
	}
}

func OperationHandleFunc(w http.ResponseWriter, r *http.Request) {
	switch method := r.Method; method {
	case http.MethodGet:
		operations := AllOperation()
		writeJSON(w, operations)
	case http.MethodPost:
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
		operation := FromJSON(body)
		operation.Date = time.Now().UTC()
		operation.Result = float64(operation.Calculate())
		CreateOperation(operation)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(operation)
		fmt.Println(operation)
	default:
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Unsupported request method."))
	}
}

func FromJSON(data []byte) Operation {
	operation := Operation{}
	err := json.Unmarshal(data, &operation)
	if err != nil {
		panic(err)
	}
	operation.Date = time.Now().UTC()
	return operation
}

func (o Operation) ToJSON() []byte {
	ToJSON, err := json.Marshal(o)
	if err != nil {
		panic(err)
	}
	return ToJSON
}

func (o Operation) Calculate() int64 {
	switch op := o.Operation; op {
	case "+":
		return int64(o.Number1) + int64(o.Number2)
	case "-":
		return int64(o.Number1) - int64(o.Number2)
	case "*":
		return int64(o.Number1) * int64(o.Number2)
	}
	return 0
}

func writeJSON(w http.ResponseWriter, i interface{}) {
	b, err := json.Marshal(i)
	if err != nil {
		panic(err)
	}
	w.Header().Add("Content-Type", "application/json; charset=utf-8")
	w.Write(b)
}

func AllOperation() []Operation {
	values := make([]Operation, len(operations))
	idx := 0
	for _, operation := range operations {
		values[idx] = operation
		idx++
	}
	return values
}
func CreateOperation(operation Operation) (string, bool) {
	_, exists := operations[operation.Date.String()]
	if exists {
		return "", false
	}
	operations[operation.Date.String()] = operation
	return operation.Date.String(), true
}
