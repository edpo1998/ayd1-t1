package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/edpo1998/ayd1-t1/api"
)

// Funcion Principal de la api
func main() {
	// root Endpoint
	operations := http.NewServeMux()
	operations.HandleFunc("/", index)
	operations.HandleFunc("/calculator", api.OperationHandleFunc)

	information := http.NewServeMux()
	information.HandleFunc("/information", api.InformationHandleFunc)

	// Start Server in Port 3000
	go func() {
		http.ListenAndServe(port(), operations)
	}()
	// Start Server in Port 4000
	http.ListenAndServe(":4000", information)
}

/* Function for PORT enviroment */
func port() string {
	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = "3000"
	}
	return ":" + port
}

/* Function for display message root Endpoint */
func index(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Welcom to AYD1 Homework #1")
}
